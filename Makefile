.PHONY: preview build build-with-drafts preview

# runs local 127.0.0.1:4000 server for preview:
build-nodocker: #favicon.ico
	bundle exec jekyll build

build-with-drafts: #favicon.ico
	bundle exec jekyll build --drafts

preview-nodocker:
	bundle exec jekyll serve --drafts
	#bundle exec jekyll serve

build: #favicon.ico
	docker run --rm -it --volume="$(PWD):/srv/jekyll"   --volume="$(PWD)/vendor/bundle:/usr/local/bundle"   -p 4000:4000 jekyll/jekyll:latest   jekyll build

build-with-drafts: #favicon.ico
	docker run --rm -it --volume="$(PWD):/srv/jekyll"   --volume="$(PWD)/vendor/bundle:/usr/local/bundle"   -p 4000:4000 jekyll/jekyll:latest   jekyll build --drafts

preview: #favicon.ico
	docker run --rm -it --volume="$(PWD):/srv/jekyll"   --volume="$(PWD)/vendor/bundle:/usr/local/bundle"   -p 4000:4000 jekyll/jekyll:latest   jekyll serve --drafts
	
preview-nodrafts: #favicon.ico
	docker run --rm -it --volume="$(PWD):/srv/jekyll"   --volume="$(PWD)/vendor/bundle:/usr/local/bundle"   -p 4000:4000 jekyll/jekyll:latest   jekyll serve

#favicon.ico: favicon.ppm
#	ppmtowinicon -output favicon.ico favicon.ppm

#favicon.ppm: assets/img/favicon_32x32.ppm
#	cp -v "$<" "$@"
